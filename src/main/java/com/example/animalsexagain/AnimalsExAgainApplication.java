package com.example.animalsexagain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnimalsExAgainApplication {

    public static void main(String[] args) {
        SpringApplication.run(AnimalsExAgainApplication.class, args);
    }

}
